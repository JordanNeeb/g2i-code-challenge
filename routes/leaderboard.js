const express = require("express");
const router = express.Router();

// @route   GET api/leaderboard
// @desc    Gets leaderboard
// @access  Public
router.get("/", (req, res) => {
  res.json({ message: "success" });
});

module.exports = router;
